#[macro_use] extern crate serenity;
#[macro_use] extern crate log;

extern crate env_logger;


use serenity::client::{Client, EventHandler};

use serenity::{
    client::bridge::gateway::{ShardId, ShardManager},
    framework::standard::{
        help_commands, Args, CommandOptions, DispatchError, HelpBehaviour, StandardFramework,
    },
    model::{channel::Message, gateway::Ready, Permissions},
    prelude::*,
    utils::{content_safe, ContentSafeOptions},
};

use std::env;

struct Handler;

impl EventHandler for Handler {}

pub fn main() {
    env_logger::init();

    println!("Staring up Kraken bot");

    // Login with a bot token from the environment
    info!("Logging in...");
    let mut client = Client::new(&env::var("DISCORD_TOKEN").expect("token"), Handler)
        .expect("Error creating client");

    info!("Setting bot prefix...");
    client.with_framework(
        StandardFramework::new()
            .configure(|c| c
                .allow_whitespace(true)
                .on_mention(true)
                .prefix("!")
                .prefix_only_cmd(about)
                .delimiters(vec![", ", ","]))
                .before(|_ctx, msg, command_name| {
                    info!("Command Received: {} - {}", command_name, msg.author.name);

                    true // if `before` returns false, command processing doesn't happen.
                })
                .after(|_, _, command_name, error| {
                    match error {
                        Ok(()) => info!("Processed command '{}'", command_name),
                        Err(why) => error!("Command '{}' returned error {:?}", command_name, why),
                    }
                })
                .unrecognised_command(|_, _, unknown_command_name| {
                    info!("Could not find command named '{}'", unknown_command_name);
                })
                .on_dispatch_error(|_ctx, msg, error| {
                    if let DispatchError::RateLimited(seconds) = error {
                        let _ = msg.channel_id.say(&format!("Try this again in {} seconds.", seconds));
                    }
                })
            .cmd("about", about)
            .cmd("ping", ping)
    );

    // start listening for events by starting a single shard
    if let Err(why) = client.start() {
        error!("An error occurred while running the client: {:?}", why);
    }
}

command!(about(_ctx, msg, _args) {
    if let Err(why) = msg.channel_id.say("
**Kraken** bot for [Discord](https://discordapp.com/)
Developed to help manage the mundane and add excitement for all.
") {
        println!("Error sending message: {:?}", why);
    }
});

command!(ping(_context, message) {
    
    let _ = message.reply("Pong!");
});